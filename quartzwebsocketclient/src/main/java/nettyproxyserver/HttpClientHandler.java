package nettyproxyserver;

import MITM.SSLCertEngineServiceImpl;
import Utils.NtlmAuth;
import com.google.gson.Gson;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.SslHandshakeCompletionEvent;
import io.netty.handler.ssl.SslCloseCompletionEvent;

import javax.net.ssl.SSLEngine;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

public class HttpClientHandler extends SimpleChannelInboundHandler<Object>{
    private IHttpSession session;
    NtlmAuth auth = new NtlmAuth("ARGUS","Administrator","pradeep@1990");
    public HttpClientHandler(){


    }
    public HttpClientHandler(IHttpSession session){
        this.session = session;

    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
       this.session.getRequestcontext().close();
       ctx.close();
       // ctx.fireExceptionCaught(cause);
    }
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg){
        System.out.println(msg);
        System.out.println(msg instanceof FullHttpResponse);
        System.out.println("--------------response received----------------");


        if(msg instanceof WebSocketFrame){

            this.session.getRequestcontext().channel().writeAndFlush(msg);
        }



      if(msg instanceof FullHttpResponse ) {
         this.session.setHttpResponse((FullHttpResponse)msg);
         System.out.println(((FullHttpResponse) msg).status().code() == 101);
         System.out.println(!this.session.getHandshaker().isHandshakeComplete());
          if (((FullHttpResponse) msg).status().code() == 101) {
              if(!this.session.getHandshaker().isHandshakeComplete()){
                  this.session.getHandshaker().finishHandshake(ctx.channel(),(FullHttpResponse)msg);
                  this.session.setResponseContext(ctx);
                  try {
                      this.srvhandleHandshake(this.session.getRequestcontext(), this.session.getFullhttpRequest());
                  }catch (Exception e){

                  }

                  ((HttpSession)this.session).setWebsocketClientChanel(ctx.channel());
              }else{
                  System.out.println("101 else block is reached");
                  try {
                      this.session.setResponseContext(ctx);
                      this.srvhandleHandshake(this.session.getRequestcontext(), this.session.getFullhttpRequest());
                  }catch (Exception e){

                  }
              }
              this.session.setResponseContext(ctx);
          }

          this.session.getRequestcontext().writeAndFlush(msg);



      }else {

          if(HttpUtil.isKeepAlive(this.session.getFullhttpRequest())){
              this.session.setResponseContext(ctx);
          }
          if(msg instanceof DefaultHttpResponse && ((DefaultHttpResponse)msg).status().code()==401 ){
              this.session.setIsauth(true);
           this.AuthHandle((DefaultHttpResponse) msg,ctx);

          }else {
              if(!this.session.isIsauth() ) {
                  this.session.getRequestcontext().channel().writeAndFlush(msg);
              }
          }

      }

    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {

    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        System.out.println("this is evt"+evt);
        if(evt instanceof SslCloseCompletionEvent){
           this.session.getRequestcontext().close();
            ctx.close();
        }

        if (evt instanceof SslHandshakeCompletionEvent) {
            SslHandshakeCompletionEvent hce = (SslHandshakeCompletionEvent) evt;

            if (!hce.isSuccess()) {
                hce.cause().printStackTrace();
                this.session.getRequestcontext().channel().close();
                ctx.close();

                return;
            }else{

               if(this.session.getFullhttpRequest().method().name().equalsIgnoreCase("connect")){


                   DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,new HttpResponseStatus(200,"Connection Established"));
                   response.headers().add(HttpHeaderNames.CONNECTION,HttpHeaderValues.KEEP_ALIVE);
                   System.out.println("writing the response ssl event is successfull");
                  // System.out.println(response);

                       SSLCertEngineServiceImpl service = new SSLCertEngineServiceImpl();
                       System.out.println("ssl-engine to create" + this.session.getUri().getHost());
                       SSLEngine engine = service.GetEngineForDomain(this.session.getUri().getHost());
                       //engine.setEnabledProtocols(new String[]{ "TLS"});
                       engine.setUseClientMode(false);
                       engine.setNeedClientAuth(false);
                       SslHandler handler = new SslHandler(engine);
                       this.session.getRequestcontext().channel().writeAndFlush(response).addListener((fut) -> {

                           if (fut.isSuccess()) {

                               System.out.println("writing connection established response to connect request");
                               this.session.getRequestcontext().channel().config().setAutoRead(true);
                               this.session.getRequestcontext().channel().pipeline().addFirst("ssl-handler", handler);

                               if(this.session.getResponseContext()==null) {
                                   //System.out.println("before resp->"+this.session.getResponseContext().channel().pipeline());
                                   this.session.setResponseContext(ctx);
                               }

                               System.out.println(this.session.getRequestcontext().channel().pipeline());
                           }
                       });








               }


            }
        }

        super.userEventTriggered(ctx, evt);
    }
    public void AuthHandle(DefaultHttpResponse response,ChannelHandlerContext ctx ){
         if(!(this.session.getNtlmState()==2)) {
             this.session.setNtlmState(1);
         }
        if(((DefaultHttpResponse)response).headers().get("WWW-Authenticate").equalsIgnoreCase("NTLM") && this.session.getNtlmState()==1){
            FullHttpRequest request = this.session.getRequestcopy();
              this.session.setRequestcopy(this.session.getRequestcopy().copy());
            request.headers().add("Authorization","NTLM "+auth.generateType1());
              ctx.writeAndFlush(request).addListener(fut->{
                  this.session.setNtlmState(2);
              });
           return;
        }
        if(this.session.getNtlmState()==2){
            FullHttpRequest request = this.session.getRequestcopy();
            this.session.setRequestcopy(this.session.getRequestcopy());
            System.out.println("------------------nounce-----------------------------------");
            System.out.println(response.headers().get("WWW-Authenticate").replace("NTLM ",""));
            System.out.println("------------------nounce-----------------------------------");
            request.headers().remove("Authorization");
            request.headers().add("Authorization","NTLM "+auth.generateType2(response.headers().get("WWW-Authenticate").replace("NTLM ","")));
            ctx.writeAndFlush(request);
            this.session.setNtlmState(0);
            this.session.setIsauth(false);
            return;
        }

    }
    public void srvhandleHandshake(ChannelHandlerContext ctx,FullHttpRequest req) throws URISyntaxException {

        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(getWebSocketURI(ctx,req),
                this.session.getFullhttpRequest().headers().get("Sec-WebSocket-Protocol"), true);
        WebSocketServerHandshaker handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);
        }


    }
    protected String getWebSocketURI(ChannelHandlerContext ctx, HttpRequest req) {
        System.out.println("webscoket context ws->"+ctx.channel().pipeline());
        System.out.println("Req URI : " + req.uri());
        String url = null;
        if(ctx.channel().pipeline().get("ssl-handler")!=null) {
            url = "wss://" + req.headers().get("Host") + req.uri();
        }else{
            url = "ws://" + req.headers().get("Host") + req.uri();
        }
        System.out.println("Constructed URL : " + url);
        return url;
    }
}
