/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nettyproxyserver;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;

import java.net.URI;

/**
 *
 * @author manju
 */
public interface IHttpSession {
    public FullHttpRequest getFullhttpRequest();
    public FullHttpResponse getFullHttpResponse();
    public HttpRequest gethttpRequest();
    public HttpResponse getHttpResponse();
    public void setHttpRequest(FullHttpRequest request);
    public void setHttpResponse(FullHttpResponse response);
    public void setRequestcontext(ChannelHandlerContext ctx);
    public void setResponseContext(ChannelHandlerContext ctx);
    public ChannelHandlerContext getRequestcontext();
    public ChannelHandlerContext getResponseContext();
    public HttpMethod getMethod();
    public HttpContent getContent();
    public void setContent(HttpContent content);
    public void setMethod(HttpMethod method);
    public WebSocketClientHandshaker getHandshaker();
    public void setHandshaker(WebSocketClientHandshaker handshaker);
    public URI getUri();
    public void setUri(URI url);
    public void setClientEventLoopGroup(EventLoopGroup group);
    public EventLoopGroup getClientEventLoopGroup();
    public FullHttpRequest getRequestcopy() ;
    public void setRequestcopy(FullHttpRequest requestcopy);
    public int getNtlmState();
    public void setNtlmState(int ntlmState) ;
    public boolean isIsauth();

    public void setIsauth(boolean isauth) ;
}
