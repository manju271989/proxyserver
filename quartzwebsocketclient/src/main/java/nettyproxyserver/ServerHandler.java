package nettyproxyserver;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;

import java.net.URI;
import java.net.URISyntaxException;

public class ServerHandler extends ChannelInboundHandlerAdapter {
    private HttpSession session;

    public ServerHandler(HttpSession session){
       this.session = session;
    }

    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelRegistered();
    }

    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelUnregistered();
    }

    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelActive();
    }

    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelInactive();
    }

    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("srv->"+msg);
        System.out.println("proxy server thread-----------------");
        System.out.println(Thread.currentThread());
        System.out.println("proxy server thread-----------------");
        //System.out.println("server handler context"+ctx.channel().pipeline());
        System.out.println(msg instanceof WebSocketFrame);
        try{
        if(msg instanceof WebSocketFrame){
            System.out.println("ws->"+msg);
            System.out.println("ws->"+this.session.getResponseContext().channel());
            System.out.println("ws->"+this.session.getUri());
            System.out.println("ws->"+this.session.getResponseContext().channel().pipeline());
            System.out.println("ws->"+this.session.getWebsocketClientChanel().isActive()+"writable"+this.session.getResponseContext().channel().isWritable());
            System.out.println("ws->"+this.session.getWebsocketClientChanel().pipeline());
            this.session.getResponseContext().channel().writeAndFlush(msg);

        }

        if(msg instanceof FullHttpRequest) {
            FullHttpRequest request = (FullHttpRequest) msg;
            this.session.setRequestcopy(request.copy());
              System.out.println("entering the block srv handler ");
            //  System.out.println("srv->request"+request);
              System.out.println("connection websocket test"+request.headers().get("Connection"));

                if (request.headers().get("Connection") != null) {

                    if (request.headers().get("Connection").equalsIgnoreCase("upgrade") ||
                            request.headers().contains("Upgrade")) {
                        System.out.println(request);
                        System.out.println("entering upgrade");
                        System.out.println("::::::::::::_____-------------->>>>>>>>>>>>>>>>>>>>>>>");
                       // handleHandshake(ctx, request);

                        session.setHttpRequest(request);
                        session.setRequestcontext(ctx);
                        this.session = session;
                        session.setUri(new URI(this.getWebSocketURI(ctx,request)));
                        HttpClient client = new HttpClient(session);
                        client.process();

                    }
                    else {
                        System.out.println("-- initial" + this.session.getUri());
                        session.setHttpRequest(request);
                        System.out.println("--uri---" + Utils.FormUri.getFullURI(request,ctx));
                        session.setUri(Utils.FormUri.getFullURI(request,ctx));
                        session.setRequestcontext(ctx);
                        this.session = session;
                        if(this.session.getResponseContext()!=null){
                            this.session.getResponseContext().channel().writeAndFlush(msg);
                        }else {
                            HttpClient client = new HttpClient(session);
                            client.process();
                        }
                    }
                } else {
                    System.out.println("-- initial" + this.session.getUri());
                    session.setHttpRequest(request);
                    System.out.println("--uri---" + Utils.FormUri.getFullURI(request,ctx));
                    session.setUri(Utils.FormUri.getFullURI(request,ctx));
                    session.setRequestcontext(ctx);
                    this.session = session;
                    if(this.session.getResponseContext()!=null && this.session.getResponseContext().channel().isWritable()){
                        this.session.getResponseContext().channel().writeAndFlush(msg);
                    }else {
                        HttpClient client = new HttpClient(session);
                        client.process();
                    }
                }

            }
        }
        catch(Exception e ){
            ctx.channel().close();
            e.printStackTrace();
        }
    }

    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelReadComplete();
    }

    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        System.out.println("THis is the event from servercontext "+evt);

    }

    public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelWritabilityChanged();
    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        this.session.getResponseContext().close();
       // ctx.fireExceptionCaught(cause);
    }
    public void srvhandleHandshake(ChannelHandlerContext ctx,FullHttpRequest req) throws URISyntaxException {

        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(getWebSocketURI(ctx,req),
                null, true);
        WebSocketServerHandshaker handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);
        }


    }
    protected String getWebSocketURI(ChannelHandlerContext ctx, HttpRequest req) {
        System.out.println("webscoket context ws->"+ctx.channel().pipeline());
        System.out.println("Req URI : " + req.uri());
        String url = null;
        if(ctx.channel().pipeline().get("ssl-handler")!=null) {
           url = "wss://" + req.headers().get("Host") + req.uri();
        }else{
            url = "ws://" + req.headers().get("Host") + req.uri();
        }
        System.out.println("Constructed URL : " + url);
        return url;
    }

}
