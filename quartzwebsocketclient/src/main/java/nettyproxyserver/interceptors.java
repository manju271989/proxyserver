package nettyproxyserver;

public interface interceptors {
    public void OnBeforeRequestToRemoteserver(IHttpSession session);
    public void OnBeforeResponseToClient(IHttpSession session);
    public void setCustomDomainName(String hostname);
    public void setModemSimulation(String Rate);
    public boolean createHAR();
}
