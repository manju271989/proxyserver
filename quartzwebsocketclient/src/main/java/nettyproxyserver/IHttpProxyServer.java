package nettyproxyserver;

import java.net.InetSocketAddress;

public interface IHttpProxyServer {
    public InetSocketAddress start();
    public void stop();

}
