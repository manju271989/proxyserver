package nettyproxyserver;

import com.google.gson.annotations.Expose;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;

import java.net.URI;
import java.net.URL;

public class HttpSession implements IHttpSession {

    private   FullHttpRequest reuqest;

    private  FullHttpResponse response;
    private  ChannelHandlerContext requestcontext;
    private  ChannelHandlerContext responsecontext;
    private  WebSocketClientHandshaker handshaker;
    private Channel websocketClientChanel;
    private ServerHandler srvhandler;
    private FullHttpRequest requestcopy;
    private boolean isauth;

    public boolean isIsauth() {
        return isauth;
    }

    public void setIsauth(boolean isauth) {
        this.isauth = isauth;
    }

    public int getNtlmState() {
        return ntlmState;
    }

    public void setNtlmState(int ntlmState) {
        this.ntlmState = ntlmState;
    }

    private int ntlmState;
    public FullHttpRequest getRequestcopy() {
        return requestcopy;
    }

    public void setRequestcopy(FullHttpRequest requestcopy) {
        this.requestcopy = requestcopy;
    }


    public ServerHandler getSrvhandler() {
        return srvhandler;
    }

    public void setSrvhandler(ServerHandler srvhandler) {
        this.srvhandler = srvhandler;
    }

    public Channel getWebsocketClientChanel() {
        return websocketClientChanel;
    }

    public void setWebsocketClientChanel(Channel websocketClientChanel) {
        this.websocketClientChanel = websocketClientChanel;
    }

    @Expose
    private URI url;

    private HttpMethod method;
    private HttpContent content;
    private EventLoopGroup clientgroup;
    private String Scheme;

    public String getScheme() {
        return Scheme;
    }

    public void setScheme(String scheme) {
        Scheme = scheme;
    }

    public String getHostName() {
        return HostName;
    }

    public void setHostName(String hostName) {
        HostName = hostName;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getFullURl() {
        return fullURl;
    }

    public void setFullURl(String fullURl) {
        this.fullURl = fullURl;
    }

    private String HostName;
    private String port;
    private String fullURl;


    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    @Override
    public WebSocketClientHandshaker getHandshaker() {
        return this.handshaker;
    }

    @Override
    public void setHandshaker(WebSocketClientHandshaker handshaker) {
      this.handshaker = handshaker;
    }

    public HttpContent getContent() {
        return content;
    }

    public void setContent(HttpContent content) {
        this.content = content;
    }

    public URI getUri() {
        try {
            return url;
        }catch(Exception e){
            return null;
        }
    }

    public void setUri(URI url) {
        this.url = url;
    }

    @Override
    public void setClientEventLoopGroup(EventLoopGroup group) {
       this.clientgroup = group;
    }

    @Override
    public EventLoopGroup getClientEventLoopGroup() {
        return this.clientgroup;
    }


    public FullHttpRequest getFullhttpRequest()
    {
        return this.reuqest;

    }
    public FullHttpResponse getFullHttpResponse(){
        return this.response;
    }
    public HttpRequest gethttpRequest(){

        return this.reuqest;
    }
    public HttpResponse getHttpResponse(){
        return this.response;
    }
    public void setHttpRequest(final FullHttpRequest request){
       // System.out.println(request);
        this.reuqest =request;
    }
    public void setHttpResponse(final FullHttpResponse response){

        this.response = response;
    }
    public void setRequestcontext(ChannelHandlerContext ctx){

        this.requestcontext = ctx;
    }
    public void setResponseContext(ChannelHandlerContext ctx){
        this.responsecontext = ctx;
    }
    public ChannelHandlerContext getRequestcontext(){
        return this.requestcontext;
    }
    public ChannelHandlerContext getResponseContext(){
        return this.responsecontext;
    }

}
