package nettyproxyserver;

import java.net.InetAddress;


public interface IHttpClient {

    public void process();
    public InetAddress resolveDomianName();
}
