package nettyproxyserver;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;


public class HttpClient  implements IHttpClient{
     private URI url;
     private boolean istocreateRequest;
     private HttpSession session = new HttpSession();
     private HttpMethod method;
     private HttpContent content;

    public HttpClient(URI url){
        this.session.setUri(url);
        this.istocreateRequest = true;
    }
    public HttpClient(URI url, HttpMethod method, HttpContent content){
        try {

            session.setMethod(method);
            session.setContent(content);
            FullHttpRequest request;
            System.out.println(url);
            if(content==null){
              request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, method, url.toString());
              request.headers().set("Host","beacons3.gvt2.com:443");
              request.headers().set("user-agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36");
              session.setUri(url);

            }else{
                request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, method, url.toString(),content.content());
            }
            session.setHttpRequest(request);

        }catch(Exception e){
            e.printStackTrace();

        }
    }
    public HttpClient(HttpSession session){

        this.session = session;
    }
    public void process(){
      try {

          Bootstrap b = new Bootstrap();
          WebSocketClientHandshaker handshaker = null;
          //  EventLoopGroup group = new NioEventLoopGroup();
          b.group(QBZProxyServer.group)
                  .channel(NioSocketChannel.class)
                  .handler(new InitHttpClientHandler(this.session));

          int port = 0;
          if (this.session.getUri().getScheme().equalsIgnoreCase("http")) {
              port = (this.session.getUri().getPort() == -1) ? 80 : this.session.getUri().getPort();
          } else if (this.session.getUri().getScheme().equalsIgnoreCase("https")) {
              port = (this.session.getUri().getPort() == -1) ? 443 : this.session.getUri().getPort();
          } else if (this.session.getUri().getScheme().equalsIgnoreCase("ws")) {
              port = (this.session.getUri().getPort() == -1) ? 80 : this.session.getUri().getPort();
              handshaker = WebSocketClientHandshakerFactory.newHandshaker(
                      this.session.getUri(), WebSocketVersion.V13, null, false, this.session.getFullhttpRequest().headers(), 1280000);
          } else if (this.session.getUri().getScheme().equalsIgnoreCase("wss")) {
              port = (this.session.getUri().getPort() == -1) ? 443 : this.session.getUri().getPort();
              handshaker = WebSocketClientHandshakerFactory.newHandshaker(
                      this.session.getUri(), WebSocketVersion.V13,this.session.getFullhttpRequest().headers().get("Sec-WebSocket-Protocol"), false, this.session.getFullhttpRequest().headers(), 1280000);
          }
          System.out.println(this.session.getUri());
          System.out.println("host: ");
          System.out.println("port: " + port);
          this.session.setClientEventLoopGroup(QBZProxyServer.group);
          b.remoteAddress(this.session.getUri().getHost(), port);
          ChannelFuture future = b.connect().syncUninterruptibly();
          Channel ch = future.awaitUninterruptibly().channel();
          System.out.println(this.session.getFullhttpRequest());
          if (this.session.getUri().getScheme().equalsIgnoreCase("https") || this.session.getUri().getScheme().equalsIgnoreCase("http")) {
              if (!this.session.getFullhttpRequest().method().name().equalsIgnoreCase("connect")) {
                  ch.writeAndFlush(session.getFullhttpRequest()).addListener((futre) -> {
                      System.out.println("---" + futre);
                  });

              }
          } else if (this.session.getUri().getScheme().equalsIgnoreCase("ws") || this.session.getUri().getScheme().equalsIgnoreCase("wss")) {
              try {
                  System.out.println("hi I am in ws block");
                  System.out.println("client ws->"+ch.pipeline());
                  System.out.println("client handshake ws->"+handshaker.handshake(ch).sync().channel().pipeline());
                  System.out.println("client pipeline ws->"+ch.pipeline());
                  //this.session.setWebsocketClientChanel(ch);

                  this.session.setHandshaker(handshaker);
              } catch (Exception e) {
                  e.printStackTrace();
                  this.session.getRequestcontext().channel().close();
              }
          }
      }catch(Exception e){
          e.printStackTrace();
          this.session.getRequestcontext().channel().close();
      }

    }
    public InetAddress resolveDomianName(){

        return null;
    }
    public static void main(String[] args){
        try {
           /* URI url = new URI("wss://localhost:58832");

            System.out.println(url.getScheme());
            System.out.println(url.getPort());
           HttpMethod method = new HttpMethod("GET");
            HttpClient client = new HttpClient(url);
            client.process();*/
          HttpClient client = new HttpClient(new URI("https://tag.getdrip.com/6914321.js"),new HttpMethod("GET"),null);
          client.process();
        }catch(Exception e){
           e.printStackTrace();

        }

    }
}
