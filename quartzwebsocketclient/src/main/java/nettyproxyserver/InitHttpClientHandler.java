package nettyproxyserver;

import MITM.SSLCertEngineServiceImpl;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.ssl.SslHandler;
import org.bouncycastle.operator.OperatorCreationException;

import javax.net.ssl.SSLEngine;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;


public class InitHttpClientHandler extends ChannelInitializer<SocketChannel> {
    private HttpSession session;
    public InitHttpClientHandler(HttpSession session){
        this.session = session;

    }

    public void channelRead(ChannelHandlerContext ctx, Object msg){
        System.out.println(msg);
    }



    @Override
    protected void initChannel(SocketChannel ch) throws Exception {

        //ch.pipeline().addLast("clientcodec",new HttpClientCodec(8192*2,8192*2,8192*2));

        //ch.pipeline().addLast("objectaggregator",new HttpObjectAggregator(8124*1024*10));
        //ch.pipeline().addLast("handler",new HttpClientHandler(this.session));
        System.out.println("-----------------------------------------testing the websocket full http response");
        System.out.println((this.session.getFullhttpRequest().headers().get("Connection") != null) && (this.session.getFullhttpRequest().headers().get("Connection").equalsIgnoreCase("Upgrade")));
      if((this.session.getFullhttpRequest().headers().get("Connection") != null) && (this.session.getFullhttpRequest().headers().get("Connection").equalsIgnoreCase("Upgrade"))){
          ch.pipeline().addLast("clientcodec",new HttpClientCodec(8192*2,8192*2,8192*2));

          ch.pipeline().addLast("objectaggregator",new HttpObjectAggregator(8124*1024*10));
          ch.pipeline().addLast("handler",new HttpClientHandler(this.session));

        }else{

          ch.pipeline().addLast("clientcodec",new HttpClientCodec(8192*2,8192*2,8192*2));

        //  ch.pipeline().addLast("objectaggregator",new HttpObjectAggregator(8124*1024*10));
          ch.pipeline().addLast("handler",new HttpClientHandler(this.session));
      }

        if(this.session.getUri().getScheme().equalsIgnoreCase("https")||this.session.getUri().getScheme().equalsIgnoreCase("wss")) {
            System.out.println("added the ssl handler");
            System.out.println("_______________________");
            SSLCertEngineServiceImpl imp = new SSLCertEngineServiceImpl();
            SSLEngine engine = imp.getSSLEngine(session.getUri().getHost(),443);
            for(String s:engine.getEnabledProtocols()){
                System.out.println("enabled protocols: "+s);
            }
            engine.setUseClientMode(true);
            SslHandler handler = new SslHandler(engine);
            ch.pipeline().addFirst("ssl",handler);
        }
        if(this.session.getUri().getScheme().equalsIgnoreCase("wss")||this.session.getUri().getScheme().equalsIgnoreCase("ws")){
           // ch.pipeline().addLast("objectaggregator",new HttpObjectAggregator(8124*1024*10));

        }


    }
    public static void main(String[] args) throws IOException, CertificateException, UnrecoverableKeyException, InvalidKeyException, SignatureException, NoSuchProviderException, OperatorCreationException, KeyStoreException, KeyManagementException {
        SSLCertEngineServiceImpl imp = new SSLCertEngineServiceImpl();

        SSLEngine engine = imp.getSSLEngine("tag.getdrip.com",443);
        engine.setUseClientMode(true);
     //   engine.beginHandshake();
        System.out.println(engine.getHandshakeStatus());

    }
}
