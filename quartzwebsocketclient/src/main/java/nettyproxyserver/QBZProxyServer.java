package nettyproxyserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.HttpClientCodec;

import java.net.InetSocketAddress;
import java.util.concurrent.ConcurrentLinkedQueue;


public class QBZProxyServer implements IHttpProxyServer {
  private final EventLoopGroup master = new NioEventLoopGroup(4);
  private final EventLoopGroup slave = new NioEventLoopGroup(8);
  public static EventLoopGroup group = new NioEventLoopGroup(8);
  public static ConcurrentLinkedQueue<SessionInfo> sessions = new ConcurrentLinkedQueue<SessionInfo>();

    private ChannelFuture channel = null;

    @Override
    public InetSocketAddress start() {

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(master,slave)
                 .channel(NioServerSocketChannel.class)
                 .childHandler(new ChannelInitializer<SocketChannel>() {
                     @Override
                     protected void initChannel(SocketChannel Channel) throws Exception {
                       Channel.pipeline().addLast("servercodec",new HttpServerCodec(8192*2,8192*2,8192*3));
                       Channel.pipeline().addLast("object aggregator",new HttpObjectAggregator(100*8124*1024));
                       Channel.pipeline().addLast("serverHandler",new ServerHandler(new HttpSession()));
                     }
                 }).option(ChannelOption.SO_BACKLOG, 5)
                .option(ChannelOption.TCP_NODELAY,true)
                .option(ChannelOption.SO_LINGER,0)
                .childOption(ChannelOption.SO_KEEPALIVE, false)
                .childOption(ChannelOption.TCP_NODELAY,true)
                .childOption(ChannelOption.SO_LINGER,0)
                .childOption(ChannelOption.CONNECT_TIMEOUT_MILLIS,1000);
        try {
            channel = bootstrap.bind(63619).awaitUninterruptibly();
        }catch(Exception e){
           e.printStackTrace();
        }
        return (InetSocketAddress) channel.channel().localAddress();


    }

    @Override
    public void stop() {
        this.master.shutdownGracefully();
        this.slave.shutdownGracefully();
        group.shutdownGracefully();
        try
        {
            channel.channel().closeFuture().sync();
        }
        catch (InterruptedException e) { }

    }
    public static void main(String[] args){

        QBZProxyServer server = new QBZProxyServer();
        System.out.println(server.start());
    }
}
