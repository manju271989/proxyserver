package Utils;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.net.URI;
import java.net.URLEncoder;

public class FormUri {

    public static String getHostName(FullHttpRequest request, ChannelHandlerContext ctx){
        return request.headers().get("Host").split(":")[0];

    }


    public static URI getFullURI(FullHttpRequest request, ChannelHandlerContext ctx){
        try {
            System.out.println("request form"+request.uri());
            URI uri = new URI(URLEncoder.encode(request.uri(),"UTF-8"));

            System.out.println(request.method().name().equalsIgnoreCase("connect"));
            if(request.method().name().equalsIgnoreCase("connect")){
                if(request.headers().get("Host") != null) {
                    uri = new URI("https://" + request.headers().get("Host") +"");
                }
                System.out.println("this is from get uri"+uri.toString());
                return uri;
            }


            if(uri.getScheme()==null){
                String scheme = "http";
                if(ctx.channel().pipeline().get("ssl-handler")!=null){
                    scheme = "https";
                }

                uri = new URI(scheme+"://"+request.headers().get("Host"));
                return uri;
            }else{
                return uri;
            }
        }
        catch(Exception e){
            e.printStackTrace();

        }
        return null;
    }


}
