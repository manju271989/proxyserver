package Utils;
import jcifs.ntlmssp.Type1Message;
import jcifs.ntlmssp.Type2Message;
import jcifs.ntlmssp.Type3Message;
import jcifs.smb.NtlmContext;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.util.Base64;

import java.io.IOException;

public class NtlmAuth {
private NtlmPasswordAuthentication ntlmauth = null;
private NtlmContext ntlmContext = null;
    public NtlmAuth(String domain,String User,String Password){
        ntlmauth = new NtlmPasswordAuthentication(domain,User,Password);
         ntlmContext = new NtlmContext(ntlmauth, true);
    }
   public String generateType1(){
       byte[] rawType1Message = new byte[0];
       try {
           rawType1Message = ntlmContext.initSecContext(new byte[]{}, 0, 0);
           Type1Message type1Message = new Type1Message(rawType1Message);
           return Base64.encode(type1Message.toByteArray());

       } catch (SmbException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
        return null;
   }
   public String generateType2(String type2nounce){
       byte[] rawType1Message = null;
       try{
           rawType1Message = ntlmContext.initSecContext(Base64.decode(type2nounce), 0, Base64.decode(type2nounce).length);
           Type3Message type3Message = new Type3Message(rawType1Message);
           return Base64.encode(type3Message.toByteArray());

       } catch (SmbException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return null;
   }
   public static void main(String[] args){
        NtlmAuth auth = new NtlmAuth("ARGUS","Administrator","pradeep@1990");
        System.out.println(auth.generateType1());
        System.out.println(auth.generateType2("TlRMTVNTUAACAAAACgAKADgAAAAFgomiEMY1S0BcrCIAAAAAAAAAAIoAigBCAAAABgOAJQAAAA9BAFIARwBVAFMAAgAKAEEAUgBHAFUAUwABABIAQQBSAEcAVQBTAC0ATABBAEIABAASAEEAUgBHAFUAUwAuAEMATwBNAAMAJgBBAFIARwBVAFMALQBMAEEAQgAuAEEAUgBHAFUAUwAuAEMATwBNAAUAEgBBAFIARwBVAFMALgBDAE8ATQAHAAgA2U2cjHh81gEAAAAA"));
   }

}
