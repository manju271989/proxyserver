package ProxyResolver;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.resolver.ResolvedAddressTypes;
import io.netty.resolver.dns.DnsNameResolver;
import io.netty.resolver.dns.DnsNameResolverBuilder;
import io.netty.resolver.dns.SingletonDnsServerAddressStreamProvider;
import io.netty.util.concurrent.Future;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

public class IPResolver {
   private DnsNameResolver resolver;

    public IPResolver(){
        EventLoopGroup group = new NioEventLoopGroup(2);
        try {
            DnsNameResolverBuilder builder = new DnsNameResolverBuilder(group.next())
                    .channelType(NioDatagramChannel.class)
                    .nameServerProvider(new SingletonDnsServerAddressStreamProvider(new InetSocketAddress("8.8.8.8",53)))
                    .resolvedAddressTypes(ResolvedAddressTypes.IPV4_PREFERRED)
                    .maxQueriesPerResolve(1)
                    .optResourceEnabled(false);
            this.resolver = builder.build();

        }catch(Exception e){
 //[www.facebook.com/157.240.23.35]

        }

    }
    public InetAddress resolve(String hostname){

        Future<InetAddress> future = this.resolver.resolve(hostname);
        try {
            return future.get(30, TimeUnit.SECONDS);
        }catch(Exception e){
            return null;
        }
    }
    public static void main(String[] args){

        IPResolver resolver = new IPResolver();
        System.out.println("-------"+resolver.resolve("www.facebook.com").getHostAddress());
        System.out.println("-------"+resolver.resolve("www.google.com").getHostAddress());
        System.out.println("-------"+resolver.resolve("www.verisign.com").getHostAddress());
       // System.out.println("-------"+resolver.resolve("localhost").getHostAddress());
    }
}
